from aws_cdk import (
    Stack,
    aws_lambda as _lambda,
    aws_apigateway as _apigw,
    aws_s3 as s3,
    aws_lambda_event_sources as eventsources,
    aws_dynamodb as dynamodb,
    aws_iam as iam,
    RemovalPolicy
    
)
from constructs import Construct

class Workshop1Stack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)
        
        my_s3 = s3.Bucket(self, "csvbucket",
            block_public_access=s3.BlockPublicAccess.BLOCK_ALL,
            encryption=s3.BucketEncryption.S3_MANAGED,
            enforce_ssl=True,
            versioned=True,
            bucket_name="cdktraining-csvbucket",
            removal_policy=RemovalPolicy.DESTROY,
            auto_delete_objects=True
        )
        
        table = dynamodb.Table(self, "montable",
            partition_key=dynamodb.Attribute(
                name="license_plate_number",
                type=dynamodb.AttributeType.STRING
            ),
            removal_policy=RemovalPolicy.DESTROY,
            table_name="vehical_data_table"
        )
        
        lambda_execution_ploicy = iam.Policy(self, 'lambdaexecutionrolepolicy',
            statements=[iam.PolicyStatement(
                actions=[
                    "s3:GetObject",
                    "dynamodb:PutItem",
                    "dynamodb:Scan"
                ],
                resources=[
                    my_s3.bucket_arn,
                    table.table_arn
                ]
            )]
        )
        
        lambda_role = iam.Role(self, "Role",
            assumed_by=iam.ServicePrincipal("lambda.amazonaws.com"),
            description="Lambda execution role",
            managed_policies=[iam.ManagedPolicy.from_aws_managed_policy_name('AWSLambdaExecute')]
            
        )

        lambda_role.attach_inline_policy(lambda_execution_ploicy)
        
        dynamodb_store_lambda = _lambda.Function(
            
            self, 'dynamodb_store_lambda',
            runtime=_lambda.Runtime.PYTHON_3_9,
            code=_lambda.Code.from_asset('dynamodb_store_lambda'),
            handler='app.handler',
            role=lambda_role,
            function_name="dynaodb_passer"
        )
        
        dynamodb_store_lambda.add_event_source(eventsources.S3EventSource(my_s3, events=[s3.EventType.OBJECT_CREATED]))
        
        dynamodb_reader_lambda = _lambda.Function(
            
            self, 'dynamodb_reader_lambda',
            runtime=_lambda.Runtime.PYTHON_3_9,
            code=_lambda.Code.from_asset('dynamodb_reader_lambda'),
            handler='app.handler',
            role=lambda_role,
            function_name="dynaodb_reader"
        )
        
        _apigw.LambdaRestApi(
            self, 'Endpoint',
            handler=dynamodb_reader_lambda,
        )