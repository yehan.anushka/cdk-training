import json
import boto3
from asyncio.log import logger

def s3_get_object(connection, bucket, key):
    try:
        response = connection.get_object(Bucket=bucket, Key=key)

        content = response['Body']

        return content

    except Exception as e:
        logger.info(e)

def store_dynamodb(info, dynamodb_table):
    
    dynamodb = boto3.client("dynamodb")
    
    for data in info:

        dynamodb.put_item(
            TableName=dynamodb_table,
            Item={
                'license_plate_number': {'S': data["licence_plate"]},  # Change 'your-unique-id' to an appropriate unique identifier
                'brand': {'S': data["brand"]},
                'model': {'S': data["model"]},
                'mileage': {'S': data["mileage"]},
                'year': {'S': data["year"]}
            }
        )
        

def handler(event, context):

    print('request: {}'.format(json.dumps(event)))
    
    s3 = boto3.client("s3")
    bucket_name = "cdktraining-csvbucket"
    dynamodb_table = "vehical_data_table"
    
    try:
    
        for records in event["Records"]:
            
            key=records["s3"]["object"]["key"]
            
            s3_object = s3_get_object(s3, bucket_name, key)
            
            json_data = json.loads(s3_object.read().decode('utf-8'))
            store_dynamodb(json_data, dynamodb_table)
        
        return {
            'statusCode': 200,
            'body': 'Data stored in DynamoDB'
        }
        
    except Exception as e:
        return {
            'statusCode': 500,
            'body': str(e)
        }