import boto3
import json

def handler(event, context):
    # DynamoDB table details
    dynamodb_table = 'vehical_data_table'

    # Initialize DynamoDB client
    dynamodb = boto3.client('dynamodb')

    try:
        # Scan the DynamoDB table to retrieve all items
        response = dynamodb.scan(
            TableName=dynamodb_table
        )

        # Check if any items were found
        if 'Items' in response:
            items = response['Items']
            extracted_data=[]
            
            for item in items:
                vehical_data={}
                vehical_data["license_plate_number"]=item["license_plate_number"]["S"]
                vehical_data["brand"]=item["brand"]["S"]
                vehical_data["model"]=item["model"]["S"]
                vehical_data["year"]=item["year"]["S"]
                extracted_data.append(vehical_data)
            # return {
            #     'statusCode': 200,
            #     'body': extracted_data
            # }
            
            return {
                'statusCode': 200,
                'headers': {
                    'Content-Type': 'application/json'
                },
                'body': json.dumps(extracted_data)
            }

        else:
            return {
                'statusCode': 404,
                'body': 'No items found'
            }
    except Exception as e:
        return {
            'statusCode': 500,
            'body': str(e)
        }
